# MONTAGNE :sunrise_over_mountains:

## Se Protéger
- Chapeau
- Crène solaire
- Trousse de soin (Sucre, sérum phisiologique, alcool, tire-tique, pansements, seconde-peau, paracétamol, sparadrap…)
- Couverture de survie
- Sifflet
- Paille flitrante : Vesteregaard Lifestraw

## Porter
- Sacs à dos : Deuter Aircontact Lite 50+10 et Simond Alpinism 33L

## S’orienter
- Boussole
- Carte IGN
- Montre : G-Shock GW-M5610

## Manger et Boire
- Gourde : Laken Futura 1,5 L avec Hydrapak Shape-Shift 3L et Camelbak
- Quarts : Vieux & Moderne…
- Couteaux : Douk Douk et Eka !
- Cuillères : Cantine…
- Briquet
- Pastilles de purification : Katadyn Micropur

## Dormir
- Tente : Ferrino Lightente 2 pro
- Matelas : Forclaz MT500 air L
- Sacs de couchage : Forclaz MT500 5°C
- Lampe frontale : Nitecore HD65 v2

## Se Nettoyer
- Gel hydroalcoolique
- Papier toilettes
- Lingettes intimes
- Pelle : Cao pelle
- Serviette : Nabaiji
- Savon
- Brosse et pâte à dents

## S’habiller
- Chaussures
- Chaussettes 
- Shorts
- T-shirts
- Polaires
- Jogging

## À réfléchir…
- Panchos
- Réchaud : Campingaz Bleuet compact & Campingaz CV 300+ :interrobang:
- Popote : Laken inox :interrobang:
- Sac de couchage : Millet Baikal 750 :interrobang:
- Housse de sac : Deuter Flight Cover 60 [:lien:](https://www.deuter.com/fr-fr/shop/accessoires/p226559-housse-etanche-et-de-transport-flight-cover-60)
